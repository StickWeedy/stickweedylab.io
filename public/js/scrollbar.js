let last_known_scroll_position = 0;
let ticking = false;
const topScrollEl = document.querySelectorAll('.top-scroll')[0];
//const documentHeight = window.innerHeight;
const documentHeight = window.scrollMaxY;
topScrollEl.setAttribute("style", `transform: scaleX(0);`);

// https://developer.mozilla.org/en-US/docs/Web/Events/scroll#Scroll_event_throttling
function doSomething(scroll_pos) {
  const percentage = scroll_pos / documentHeight;
  console.log({percentage}, {scroll_pos}, {documentHeight});
  topScrollEl.setAttribute("style", `transform: scaleX(${percentage});`);
}

window.addEventListener('scroll', function(e) {
  last_known_scroll_position = window.scrollY;

  if (!ticking) {
    window.requestAnimationFrame(function() {
      doSomething(last_known_scroll_position);
      ticking = false;
    });

    ticking = true;
  }
});